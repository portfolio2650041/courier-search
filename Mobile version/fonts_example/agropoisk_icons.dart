// Place fonts/agropoisk.ttf in your fonts/ directory and
// add the following to your pubspec.yaml
// flutter:
//   fonts:
//    - family: agropoisk
//      fonts:
//       - asset: fonts/agropoisk.ttf
import 'package:flutter/widgets.dart';

class Agropoisk {
  Agropoisk._();

  static const String _fontFamily = 'agropoisk';

  static const IconData diamond = IconData(0xe94d, fontFamily: _fontFamily);
  static const IconData union = IconData(0xe94c, fontFamily: _fontFamily);
  static const IconData beacon = IconData(0xe947, fontFamily: _fontFamily);
  static const IconData structure = IconData(0xe948, fontFamily: _fontFamily);
  static const IconData plus2 = IconData(0xe949, fontFamily: _fontFamily);
  static const IconData lightning = IconData(0xe94a, fontFamily: _fontFamily);
  static const IconData box1 = IconData(0xe94b, fontFamily: _fontFamily);
  static const IconData grid = IconData(0xe946, fontFamily: _fontFamily);
  static const IconData tick2 = IconData(0xe944, fontFamily: _fontFamily);
  static const IconData bin = IconData(0xe945, fontFamily: _fontFamily);
  static const IconData question_mark = IconData(0xe943, fontFamily: _fontFamily);
  static const IconData building = IconData(0xe941, fontFamily: _fontFamily);
  static const IconData build = IconData(0xe942, fontFamily: _fontFamily);
  static const IconData camera = IconData(0xe91c, fontFamily: _fontFamily);
  static const IconData tick = IconData(0xe917, fontFamily: _fontFamily);
  static const IconData whatsapp = IconData(0xe916, fontFamily: _fontFamily);
  static const IconData circle_search = IconData(0xe919, fontFamily: _fontFamily);
  static const IconData search = IconData(0xe940, fontFamily: _fontFamily);
  static const IconData lock = IconData(0xe93f, fontFamily: _fontFamily);
  static const IconData express = IconData(0xe927, fontFamily: _fontFamily);
  static const IconData adress_in = IconData(0xe93e, fontFamily: _fontFamily);
  static const IconData eye2 = IconData(0xe93d, fontFamily: _fontFamily);
  static const IconData time = IconData(0xe901, fontFamily: _fontFamily);
  static const IconData time_bold = IconData(0xe906, fontFamily: _fontFamily);
  static const IconData circle_bold = IconData(0xe909, fontFamily: _fontFamily);
  static const IconData circle = IconData(0xe914, fontFamily: _fontFamily);
  static const IconData cross_eye = IconData(0xe915, fontFamily: _fontFamily);
  static const IconData arrow = IconData(0xe93c, fontFamily: _fontFamily);
  static const IconData person = IconData(0xe900, fontFamily: _fontFamily);
  static const IconData human = IconData(0xe902, fontFamily: _fontFamily);
  static const IconData child = IconData(0xe903, fontFamily: _fontFamily);
  static const IconData setting_vector = IconData(0xe904, fontFamily: _fontFamily);
  static const IconData signature = IconData(0xe905, fontFamily: _fontFamily);
  static const IconData notification = IconData(0xe907, fontFamily: _fontFamily);
  static const IconData rubl = IconData(0xe908, fontFamily: _fontFamily);
  static const IconData eye = IconData(0xe90a, fontFamily: _fontFamily);
  static const IconData star = IconData(0xe90b, fontFamily: _fontFamily);
  static const IconData clover = IconData(0xe90c, fontFamily: _fontFamily);
  static const IconData baket_new = IconData(0xe90d, fontFamily: _fontFamily);
  static const IconData cross = IconData(0xe90e, fontFamily: _fontFamily);
  static const IconData map = IconData(0xe90f, fontFamily: _fontFamily);
  static const IconData cloud = IconData(0xe910, fontFamily: _fontFamily);
  static const IconData plus = IconData(0xe911, fontFamily: _fontFamily);
  static const IconData check_mark = IconData(0xe912, fontFamily: _fontFamily);
  static const IconData rub = IconData(0xe913, fontFamily: _fontFamily);
  static const IconData letter = IconData(0xe918, fontFamily: _fontFamily);
  static const IconData call = IconData(0xe91a, fontFamily: _fontFamily);
  static const IconData telegram = IconData(0xe91b, fontFamily: _fontFamily);
  static const IconData viber = IconData(0xe91d, fontFamily: _fontFamily);
  static const IconData info = IconData(0xe91e, fontFamily: _fontFamily);
  static const IconData setting = IconData(0xe91f, fontFamily: _fontFamily);
  static const IconData bookmark = IconData(0xe920, fontFamily: _fontFamily);
  static const IconData stuff = IconData(0xe921, fontFamily: _fontFamily);
  static const IconData graph = IconData(0xe922, fontFamily: _fontFamily);
  static const IconData dollar = IconData(0xe923, fontFamily: _fontFamily);
  static const IconData tab = IconData(0xe924, fontFamily: _fontFamily);
  static const IconData checklist = IconData(0xe925, fontFamily: _fontFamily);
  static const IconData screen = IconData(0xe926, fontFamily: _fontFamily);
  static const IconData arrow_back = IconData(0xe928, fontFamily: _fontFamily);
  static const IconData footprints = IconData(0xe929, fontFamily: _fontFamily);
  static const IconData track = IconData(0xe92a, fontFamily: _fontFamily);
  static const IconData car = IconData(0xe92b, fontFamily: _fontFamily);
  static const IconData car2 = IconData(0xe92c, fontFamily: _fontFamily);
  static const IconData feature = IconData(0xe92d, fontFamily: _fontFamily);
  static const IconData feature2 = IconData(0xe92e, fontFamily: _fontFamily);
  static const IconData box = IconData(0xe92f, fontFamily: _fontFamily);
  static const IconData big_box = IconData(0xe930, fontFamily: _fontFamily);
  static const IconData furniture = IconData(0xe931, fontFamily: _fontFamily);
  static const IconData tools = IconData(0xe932, fontFamily: _fontFamily);
  static const IconData list_bold = IconData(0xe933, fontFamily: _fontFamily);
  static const IconData card_box = IconData(0xe934, fontFamily: _fontFamily);
  static const IconData clothes = IconData(0xe935, fontFamily: _fontFamily);
  static const IconData list = IconData(0xe936, fontFamily: _fontFamily);
  static const IconData blazer = IconData(0xe937, fontFamily: _fontFamily);
  static const IconData package = IconData(0xe938, fontFamily: _fontFamily);
  static const IconData packet = IconData(0xe939, fontFamily: _fontFamily);
  static const IconData basket = IconData(0xe93a, fontFamily: _fontFamily);
  static const IconData food = IconData(0xe93b, fontFamily: _fontFamily);
}
