const {
	watch,
	parallel,
	series
} = require('gulp');

module.exports = function watching() {
	watch('src/**/*.html', parallel('html'));
	watch('src/**/*.scss', parallel('style'));
	watch('src/**/*.json', parallel('html'));
	watch('src/**/*.html', parallel('html_m'));
	watch('src/**/*.scss', parallel('style_m'));
	watch('src/**/*.json', parallel('html_m'));
}