const bs = require('browser-sync');

module.exports = function bs_html_m() {
	bs.init({
		server: {
			baseDir: 'build/mobile/',
			host: '127.0.0.1',
			directory: true,
		},
		browser: 'chrome',
		logPrefix: 'BS-HTML:',
		logLevel: 'info',
		logConnections: true,
		logFileChanges: true,
		open: true
	})
}