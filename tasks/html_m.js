const {
	src,
	dest
} = require('gulp');
const include = require('gulp-file-include');
const bs = require('browser-sync');

module.exports = function html_m() {
	return src(['src/Pages/mobile_version/**/*.html', 'src/components/elements_mobile/**/*.html', '!src/components/**/*.html', '!src/const/**/*.html'])
		.pipe(include())
		.pipe(dest('build/mobile/'))
    .pipe(bs.stream())
}