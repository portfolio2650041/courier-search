const gulp = require('gulp');
const requireDir = require('require-dir');
const tasks = requireDir('./tasks');

exports.style = tasks.style;
exports.style_m = tasks.style_m;
exports.html = tasks.html;
exports.html_m = tasks.html_m;
exports.bs_html = tasks.bs_html;
exports.bs_html_m = tasks.bs_html_m;
exports.watch = tasks.watch;

exports.mobile = gulp.parallel(
	exports.style_m,
	exports.html_m,
	exports.bs_html_m,
	exports.watch
)

exports.default = gulp.parallel(
	exports.style,
	exports.html,
	exports.bs_html,
	exports.watch
)